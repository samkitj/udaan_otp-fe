import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class Browser {

  final apiGatewayURL = "https://z9m069ovyf.execute-api.ap-south-1.amazonaws.com/prod/get_app_login_details/";

  Future<String> sendOTP(otp) async {
    //http post method to get the videoLinks
    http.Response res = await http.post(apiGatewayURL,
        body: convert.jsonEncode(<String, String>{
          'otp': otp,
          'vendor': 'udaan'
        }),
        headers:<String, String>{
          "User-Agent": "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0"
        });

    return res.body;
  }
}

