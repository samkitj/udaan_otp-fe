import 'package:flutter/material.dart';
import 'package:ninjacartotp/pages/LoadingSpinkit.dart';
import 'package:ninjacartotp/pages/URLTextField.dart';
import 'package:ninjacartotp/utils/browser.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  var otp ="";
  var browser = new Browser();
  var loading = false;

  OverlayEntry _overlayLoadingEntry;

  //callback to set videoURL data
  void onOtpChange(otp) {
    this.otp = otp;
  }

  insertOverlayLoadingEntry(BuildContext context) {
    this._overlayLoadingEntry = OverlayEntry(
        builder: (context)=>LoadingSpinkit(),
        opaque: false
    );
    Overlay.of(context).insert(this._overlayLoadingEntry);
  }

  //fetches the video link fetch
  void onSendOTP(BuildContext context) async{

    //insert loading overlay entry on the current overlay
    this.insertOverlayLoadingEntry(context);

    String currOTP =this.otp;

    if(currOTP.length>0) {
      //fetch the links from the backend
      String otpSentResponse= await browser.sendOTP(currOTP);

      //remove overlay entry from the overlay stack of the context
      this._overlayLoadingEntry.remove();
      this.otp="";
      this.setState(() {});

      if(otpSentResponse=="Success") {
        showAlertDialogBox(context,"OTP sent successfully");
      }
      else {
        showAlertDialogBox(context,"OTP send failed!!!!!");
      }

    } else {
      this._overlayLoadingEntry.remove();
      showAlertDialogBox(context,"Please enter a valid OTP");
    }
  }


  void showAlertDialogBox(BuildContext context,String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Alert"),
          content: new Text(content),
          actions: <Widget>[
            new FlatButton(
                onPressed: ()=> Navigator.of(context).pop(),
                child: new Text("Close")
            )
          ],
        );
      }
    );
  }


  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [
                0.1,0.8
              ],
              colors: [Colors.grey,Colors.lightGreen]
            )
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Padding(
              padding: const EdgeInsets.all(5),
              child: Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 10,),
                    URLTextField(otp: this.otp,onSendButtonPress: this.onSendOTP,otpChangeCallback: this.onOtpChange),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
